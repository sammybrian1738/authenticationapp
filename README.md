# installing the project in your ubuntu server
N/B : Whenever <...> in a command is used means your enter word/name of your choosing

git clone https://sammybrian1738@bitbucket.org/sammybrian1738/authenticationapp.git

# installing DB of choose
Curently the project works with two DBMS, sqlite and postgresql

To install postgresql on you machine, do the following:
    - Run sudo apt-get update
    - sudo apt-get install postgresql postgresql-contrib
    - To check if postgresql works, run service postgresql to get list of commands then run each one 
      depending on what you want
    - by default user for postgresql is postgres with no password, to create new user:
          - run sudo su postgres, then psql to get into the postgresql shell
          - then run \du to list users
          - CREATE USER <new_user> WITH PASSWORD '<user_password>'
          - to give the new user privileges, run ALTER USER <new_user> WITH PRIVILEGES
          - to install pgadmin, go to software app in ubuntu, search pgadmin then install
          - after installation you can open pgadmin to browse
    - to create db in pgadmin, sign in as default user, that is, postgres, then create DB, dbname of you choose,
      just make sure the dbname, user, password matches the one used in settings.py file of django project under 
      DATABASES.

TO install sqlite browser, go to terminal run: 
    - sudo apt-get update
    - sudo apt-get install sqlitebrowser
    - sqlitebrowser to open it

# Configuring your db connection
Depending on which database you want to use, go to settings.y file of you django project, comment out the database
configurations you wish to use

# installing the dependencies
First, ensure you've got python and pip installed
If not run the following commands:
    sudo apt-get update
    sudo apt install python3.8
    python ––version
    sudo apt install python3-pip

Now that you have python and pip installed, install virtual environment
    - sudo apt-get install python3-venv
    - create directory in you home directory called say env using, mkdir env in the terminal 
      while you are in your home directory
    - In the terminal still while you in your home directory run the following command:
        python3 -m venv /path/to/your/env directory you created/<your venv name, say, my-venv>
    - While still in your home directory, run sudo apt-get install libpq-dev, the run
      sudo apt-get install python3-psycopg2, to solve errors you might run into when pip installing the dependencies
    - enter into the django directory you just cloned and stay there
    - Run source ~/env/<venv name>/bin/activate   to activate your venv

Now that the virtual environment is up and running run the following command:
    - pip3 install -r requirements.txt
    - if you get errors, psycopg2 failed install, run pip3 install psycopg2 by itself to install psycopg2, then
      run pip3 install -r requirements.txt
    - Run python manage.py makemigrations
    - then run python manage.py migrate to migrate to your DB
    - Run python manage.py runserver to run the server

# installing uwsgi, nginx
Open new terminal to avoid exiting the venv
To install uwsgi run:
    - sudo apt-get install python3.8-dev
    - sudo apt-get install gcc
    - In your venv terminal run pip install uwsgi
    - to check if the pages are served, in the venv terminal run
      uwsgi --http :8000 --module authentication.uwsgi where authentication is the main appname of the django project

TO install nginx run the following:
    - sudo apt-get install nginx in the normal terminal
    - Create configuration file for your django project in nginx, while in venv terminal cd to /etc/nginx/sites-available
      then sudo vim authenticationapp.conf
    - While in authenticationapp.conf add the following:
        upstream django {
            server unix:///path/to/your/django/project/authenticationapp.sock;
        }
        server {
            listen 8080;
            server_name <names of your allowed hosts separated with space if more that one>;
            charset utf-8;
            client_max_body_size 75M;
            location /media {
                alias /path/to/your/djangoapp/media;
            }
            location /static {
                alias /path/to/your/djangoapp/static;
            }
            location / {
                uwsgi_pass django;
                include /path/to/your/djangoapp/uwsgi_params;
            }
        }

To link sites-available to sites-enabled:
    - run sudo ln -s /etc/nginx/sites-available/authenticationapp.conf /etc/nginx/sites-enabled/ in you normal terminal

To start nginx service run sudo /etc/init.d/nginx restart in your normal terminal

To collect your static files:
    - in your venv run python manage.py collectstatic

To connect nginx, uwsgi, and django run :
    - uwsgi --socket authenticationapp.sock --module authentication.wsgi --chmod-socket=666

At this point you are good to go. For more information go to:
https://uwsgi-docs.readthedocs.io/en/latest/tutorials/Django_and_nginx.html
    


