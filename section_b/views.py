from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.http import HttpResponseRedirect
from django.shortcuts import render, redirect
from django.contrib.auth import login, authenticate
from django.contrib.auth.forms import UserCreationForm
from .forms import RegisterForm


# Create your views here.
def register(request):
    if request.method == 'POST':
        form = RegisterForm(request.POST)
        if form.is_valid():
            new_user = form.save()
            messages.info(request, "Thanks for registering. You are now logged in.")
            new_user = authenticate(username=form.cleaned_data['username'],
                                    password=form.cleaned_data['password1'],
                                    )
            login(request, new_user)
            return HttpResponseRedirect("/login_success")
    else:
        form = RegisterForm()

    return render(request, 'register/register.html', {'form': form})


def home(request):
    return render(request, 'home/home.html')


@login_required
def login_success(request):
    current_user = request.user
    user_name = current_user.username
    return render(request, 'home/success.html', {'username': user_name})

