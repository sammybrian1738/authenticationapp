from django.apps import AppConfig


class SectionBConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'section_b'
